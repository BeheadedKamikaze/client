package com.sheepit.client.hardware.gpu.nvidia;

import java.util.ArrayList;
import java.util.List;

import com.sheepit.client.hardware.gpu.GPUDevice;
import com.sheepit.client.hardware.gpu.GPULister;
import com.sheepit.client.os.OS;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.LongByReference;

public class Nvidia implements GPULister {
	public static String TYPE = "OPTIX";
	
	//https://docs.blender.org/manual/en/3.3/render/cycles/gpu_rendering.html#optix-nvidia
	private static final String MINIMUM_DRIVER_VERSION = "470";
	
	@Override public List<GPUDevice> getGpus() {
		OS os = OS.getOS();
		String path = os.getCUDALib();
		if (path == null) {
			return null;
		}
		CUDA cudalib = null;
		try {
			cudalib = (CUDA) Native.load(path, CUDA.class);
		}
		catch (java.lang.UnsatisfiedLinkError e) {
			return null;
		}
		catch (java.lang.ExceptionInInitializerError e) {
			System.out.println("Nvidia::getGpus ExceptionInInitializerError " + e);
			return null;
		}
		catch (Exception e) {
			System.out.println("Nvidia::getGpus generic exception " + e);
			return null;
		}
		
		int result = CUresult.CUDA_ERROR_UNKNOWN;
		
		result = cudalib.cuInit(0);
		if (result != CUresult.CUDA_SUCCESS) {
			System.out.println("Nvidia::getGpus cuInit failed (ret: " + result + ")");
			if (result == CUresult.CUDA_ERROR_UNKNOWN) {
				System.out.println("If you are running Linux, this error is usually due to nvidia kernel module 'nvidia_uvm' not loaded.");
				System.out.println("Relaunch the application as root or load the module.");
				System.out.println("Most of time it does fix the issue.");
			}
			return null;
		}
		
		if (result == CUresult.CUDA_ERROR_NO_DEVICE) {
			return null;
		}
		
		IntByReference count = new IntByReference();
		result = cudalib.cuDeviceGetCount(count);
		
		if (result != CUresult.CUDA_SUCCESS) {
			System.out.println("Nvidia::getGpus cuDeviceGetCount failed (ret: " + CUresult.stringFor(result) + ")");
			return null;
		}
		
		List<GPUDevice> devices = new ArrayList<>(count.getValue());
		
		for (int num = 0; num < count.getValue(); num++) {
			IntByReference aDevice = new IntByReference();
			
			result = cudalib.cuDeviceGet(aDevice, num);
			if (result != CUresult.CUDA_SUCCESS) {
				System.out.println("Nvidia::getGpus cuDeviceGet failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			
			IntByReference computeCapabilityMajor = new IntByReference();
			result = cudalib.cuDeviceGetAttribute(computeCapabilityMajor, CUDeviceAttribute.CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, aDevice.getValue());
			if (result != CUresult.CUDA_SUCCESS) {
				System.out
					.println("Nvidia::getGpus cuDeviceGetAttribute for CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			
			if (computeCapabilityMajor.getValue() < 5) {	//Card is Maxwell or older, too old for optix
				continue;
			}
			
			
			IntByReference pciDomainId = new IntByReference();
			IntByReference pciBusId = new IntByReference();
			IntByReference pciDeviceId = new IntByReference();
			result = cudalib.cuDeviceGetAttribute(pciDomainId, CUDeviceAttribute.CU_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID, aDevice.getValue());
			if (result != CUresult.CUDA_SUCCESS) {
				System.out
						.println("Nvidia::getGpus cuDeviceGetAttribute for CU_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			result = cudalib.cuDeviceGetAttribute(pciBusId, CUDeviceAttribute.CU_DEVICE_ATTRIBUTE_PCI_BUS_ID, aDevice.getValue());
			if (result != CUresult.CUDA_SUCCESS) {
				System.out.println("Nvidia::getGpus cuDeviceGetAttribute for CU_DEVICE_ATTRIBUTE_PCI_BUS_ID failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			result = cudalib.cuDeviceGetAttribute(pciDeviceId, CUDeviceAttribute.CU_DEVICE_ATTRIBUTE_PCI_DEVICE_ID, aDevice.getValue());
			if (result != CUresult.CUDA_SUCCESS) {
				System.out
						.println("Nvidia::getGpus cuDeviceGetAttribute for CU_DEVICE_ATTRIBUTE_PCI_DEVICE_ID failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			
			byte name[] = new byte[256];
			
			result = cudalib.cuDeviceGetName(name, 256, num);
			if (result != CUresult.CUDA_SUCCESS) {
				System.out.println("Nvidia::getGpus cuDeviceGetName failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			
			LongByReference ram = new LongByReference();
			try {
				result = cudalib.cuDeviceTotalMem_v2(ram, num);
			}
			catch (UnsatisfiedLinkError e) {
				// fall back to old function
				result = cudalib.cuDeviceTotalMem(ram, num);
			}
			
			if (result != CUresult.CUDA_SUCCESS) {
				System.out.println("Nvidia::getGpus cuDeviceTotalMem failed (ret: " + CUresult.stringFor(result) + ")");
				return null;
			}
			
			NVML nvml = null;
			try {
				nvml = Native.load(os.getNVMLLib(), NVML.class);
			}
			catch (UnsatisfiedLinkError e) {
				System.out.println("Nvidia::getGpus failed to load NVML library");
				return null;
			}
			
			result = nvml.nvmlInit_v2();
			if (result != NVMLResult.NVML_SUCCESS) {
				System.out.println("Nvidia::getGpus failed to nvmlInit failed. Returned " + result);
				return null;
			}
			
			short stringLength = 80;
			byte[] driverStringBuffer = new byte[stringLength];
			result = nvml.nvmlSystemGetDriverVersion(driverStringBuffer, stringLength); //The returned char* will never exceed 80 characters according to the docs
			
			if (result != NVMLResult.NVML_SUCCESS) {
				System.out.println("Nvidia::getGpus failed to retrieve driver version");
				nvml.nvmlShutdown();
				return null;
			}
			
			nvml.nvmlShutdown();
			
			String driverVersion = (new String(driverStringBuffer)).trim();
			boolean driverTooOld = GPUDevice.compareVersions(driverVersion, MINIMUM_DRIVER_VERSION) < 0;
			if (driverTooOld) {
				System.out.println("Nvidia::getGpus driver version: " + driverVersion + " is too old. Please update to version: " + MINIMUM_DRIVER_VERSION + " or newer." );
				return null;
			}
			
			String blenderId = String
					.format("CUDA_%s_%04x:%02x:%02x_OptiX", new String(name).trim(), pciDomainId.getValue(), pciBusId.getValue(), pciDeviceId.getValue());
			GPUDevice gpu = new GPUDevice(TYPE, new String(name).trim(), ram.getValue(), blenderId);
			gpu.setDriverVersion(driverVersion);
			// for backward compatibility generate a CUDA_N id
			gpu.setOldId(TYPE + "_" + num);
			devices.add(gpu);
		}
		
		return devices;
	}
}
