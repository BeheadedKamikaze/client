/*
 * Copyright (C) 2010-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.hardware.cpu;

import com.sheepit.client.Log;
import com.sheepit.client.os.OS;
import com.sheepit.client.os.Windows;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CPU {
	public static final int MIN_CORES = Runtime.getRuntime().availableProcessors() > 1 ? 2 : 1;
	private String name;
	private String model;
	private String family;
	private String arch; // 32 or 64 bits
	private static final Log log = Log.getInstance(null);
	private static Integer logicalCores = -1; // Store core count throughout instances
	
	public CPU() {
		this.name = null;
		this.model = null;
		this.family = null;
		this.generateArch();
	}
	
	public String name() {
		return this.name;
	}
	
	public String model() {
		return this.model;
	}
	
	public String family() {
		return this.family;
	}
	
	public String arch() {
		return this.arch;
	}
	
	public int cores() {
		// If logicalCores is -1, then haven't run the core-checking script yet
		// Only want to run it once since cores is called multiple times
		if(logicalCores == -1) {
			// Use normal method for systems other than windows, and in case script fails.
			logicalCores = Runtime.getRuntime().availableProcessors();
			
			if (OS.getOS() instanceof Windows) {
				ProcessBuilder powershellProcess = new ProcessBuilder("powershell.exe",
					"((Get-CimInstance -ClassName Win32_Processor).NumberOfLogicalProcessors | Measure-Object -Sum ).Sum");
				powershellProcess.redirectErrorStream(true);
				Process process = null;
				
				// Initiate Process and catch any possible IO error
				try {
					process = powershellProcess.start();
				} catch(IOException ex) {
					log.error("CPU::cores Error starting Powershell Process. Stacktrace:" + ex);
				}
				
				if (process != null) {
					
					// Using try with resources. Will automatically close the BufferedReader stream
					try (BufferedReader shellStdOut = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
						String shellLine;
						
						while ((shellLine = shellStdOut.readLine()) != null) {
							// Verify output is a number, and if not print it out since it has errored
							if (shellLine.matches("^[+-]?\\d+$")) {
								logicalCores = Integer.parseInt(shellLine);
							}
							else {
								log.error("CPU::cores Powershell error: " + shellLine);
							}
						}
						
						process.waitFor();
					}
					catch (Exception ex) {
						log.error("CPU::cores Error in Powershell Script. Stacktrace: " + ex);
					}
					finally {
						// Destroy process to prevent memory leaks
						process.destroy();
					}
				}
				else {
					log.error("CPU::cores Error powershell process is NULL! Cannot run script.");
				}
			}
		}
		return logicalCores;
	}
	
	public void setName(String name_) {
		this.name = name_;
	}
	
	public void setModel(String model_) {
		this.model = model_;
	}
	
	public void setFamily(String family_) {
		this.family = family_;
	}
	
	public void setArch(String arch_) {
		this.arch = arch_;
	}
	
	public void generateArch() {
		String arch = System.getProperty("os.arch").toLowerCase();
		switch (arch) {
			case "i386":
			case "i686":
			case "x86":
				this.arch = "32bit";
				break;
			case "amd64":
			case "x86_64":
			case "x64":
			case "aarch64":
				this.arch = "64bit";
				break;
			default:
				this.arch = null;
				break;
		}
	}
	
	public boolean haveData() {
		return this.name != null && this.model != null && this.family != null && this.arch != null;
	}
	
}
